# Trabalho Final de IA Conexionista



## Código: Final.py

[Final.py](https://gitlab.com/andre.brascher/trabalho-final-de-ia-conexionista/-/blob/main/final.py)


## Experimentos:
Pasta com os resultados de experimentos realizados: [https://www.dropbox.com/sh/i6gulmhjf09wl2s/AADndD7R3sfbL8FKoyED55V_a?dl=0](https://www.dropbox.com/sh/i6gulmhjf09wl2s/AADndD7R3sfbL8FKoyED55V_a?dl=0)

- As configurações usadas em cada experimentos estão listadas nos respectivos arquivos run.log, que se encontram nas pastas de cada experimento;
- Além disso, em cada pasta se encontram os históricos de métricas de treinamento, juntamente com uma subpasta contendo quadros gerados.

A pasta geral de resultados de experimentos está subdividida da seguinte forma:
1. auto1D: resultados com autoenconders com camadas convolucionais 1-D
2. auto2D: resultados com autoenconders com camadas convolucionais 2-D
3. Frames: uma execução, apenas gerando exemplos de quadros de saída para os conjuntos de treino, validação e testes (utilizado na apresentação para demonstrar quadros de saída utilizados)
4. interpol: pasta com os resultados apresentados (além de outros casos). As redes discutidas na apresentação são as chamadas de sep_conv\* no caso das interpolações com 2 quadros de entrada e quad\* no caso das interpolações com 3 quadros de entrada
