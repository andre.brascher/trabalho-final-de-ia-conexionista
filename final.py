import os
os.system('cls' if os.name=='nt' else 'clear')

import io

import sys
from pathlib import Path
import shutil
from contextlib import redirect_stdout



import pandas as pd
import numpy as np
import math
# import itertools
import random

from sklearn import preprocessing
# from sklearn.metrics import confusion_matrix
# from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.utils import class_weight


# import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam




import matplotlib.pyplot as plt
import seaborn as sns

import time

import y4m





VIDEO_DIRECTORY = '/run/media/andre/ExtraSpace/docker_images/images_volumes/cuda11_custom/RAW/lower_res/videos/cif/'

# VIDEO_LIST = ["akiyo_cif"]
# VIDEO_LIST = ["bridge_far_cif"]
# VIDEO_LIST = ["akiyo_cif", "bridge_far_cif"]
VIDEO_LIST = ["akiyo_cif", "bowing_cif", "bridge_close_cif", "bridge_far_cif", "bus_cif", "city_cif", "coastguard_cif", "container_cif", "crew_cif", "deadline_cif", "flower_cif", "football_b_cif", "foreman_cif", "hall_monitor_cif", "harbour_cif", "highway_cif", "husky_cif", "ice_cif", "mad900_cif", "mobile_cif", "mother_daughter_cif", "news_cif", "pamphlet_cif", "paris_cif", "sign_irene_cif", "silent_cif", "soccer_cif", "students_cif", "tempete_cif", "waterfall_cif"]



TRAINING_SIZE = 18
VALIDATING_SIZE = 4
TESTING_SIZE = 8
Y_SHAPE = (288, 352)






# ===============================================================================================
# ===============================================================================================
# ============================== EXECUTION PARAMETERS: ==========================================


WAIT_PLOTS = False # Whether to wait for the plots to be closed before ending the run
SCALING = True # Whether to apply scaling over the pixel values
INTERPOL_MODE = True # Whether perform interpolation
# INTERPOL_MODE = None # Whether perform interpolation


# ===============================================================================================
# ===============================================================================================
# ===============================================================================================









# ===============================================================================================
# ===============================================================================================
# ================================= MODEL PARAMETERS: ===========================================


# lr = 1
# lr = 0.001 # initial
lr = 0.0001
# lr = 0.00001

# loss_funct = 'categorical_crossentropy'
# loss_funct = 'binary_crossentropy'
loss_funct = 'mean_squared_error'

# activ_funct = "relu"
activ_funct = "tanh"
# activ_funct = "sigmoid"

recurr_activ_funct = "sigmoid"

activ_last_funct = "softmax"
# activ_last_funct = "relu"
# activ_last_funct = None

droput_rate = 0.2
# droput_rate = 0.3
# droput_rate = 0.4
# droput_rate = 0.5
# droput_rate = 0.6

# epochs_val = 10
# epochs_val = 25
# epochs_val = 50
# epochs_val = 100
# epochs_val = 200
# epochs_val = 400
# epochs_val = 500
epochs_val = 1000
# epochs_val = 5000
# epochs_val = 10000
# epochs_val = 20000

# batch_size_val = 20
# batch_size_val = 32
# batch_size_val = 64 # initial value
batch_size_val = TRAINING_SIZE

# monitor_str = 'val_loss'
monitor_str = 'mean_squared_error'

# patience_val = 100
# patience_val = 125
# patience_val = 150
# patience_val = 200
# patience_val = 250
# patience_val = 300
patience_val = 500
# patience_val = 1000



INPUT_SHAPE = None
# if INTERPOL_MODE: INPUT_SHAPE = (Y_SHAPE[0], Y_SHAPE[1], 2)
if INTERPOL_MODE: INPUT_SHAPE = (Y_SHAPE[0], Y_SHAPE[1], 3)
else: INPUT_SHAPE = (Y_SHAPE[0], Y_SHAPE[1], 2)

OUTPUT_SHAPE = (Y_SHAPE[0], Y_SHAPE[1])

FRAME_SIZE = Y_SHAPE[0] * Y_SHAPE[1]



# ===============================================================================================
# ===============================================================================================
# ===============================================================================================





pathstr = "test/"
path_tests_obj = Path(pathstr)
shutil.rmtree(path_tests_obj, ignore_errors=True)
path_tests_obj.mkdir(exist_ok=True)

frames_pathstr = "test/frames/"
path_frames_obj = Path(frames_pathstr)
shutil.rmtree(path_frames_obj, ignore_errors=True)
path_frames_obj.mkdir(exist_ok=True)


logfile = open(pathstr + "run.log", mode = 'w')







def frame_420_to_np(frame):
	if frame.headers['C'] != "420jpeg" and frame.headers['C'] != "420mpeg2":
		print("\n\n\nERROR: FRAME HEADER MISMATCH!!!\n")
		print(frame)
		print(frame.headers)
		assert(False)

	frame_number = frame.count
	H = frame.headers['H']
	W = frame.headers['W']
	SIZEY = H * W # 288 * 352
	SIZECb = int(H/2 * W/2) # 352/2 * 288/2
	SIZECr = int(H/2 * W/2) # 352/2 * 288/2

	pixels = list(frame.buffer)
	Y = pixels[:SIZEY]
	Cb = pixels[SIZEY:SIZEY+SIZECb]
	Cr = pixels[SIZEY+SIZECb:]

	npY = np.array(Y)
	npCb = np.array(Cb)
	npCr = np.array(Cr)

	Y_shape = (H, W)
	Chroma_shape = (int(H/2), int(W/2))
	return frame_number, npY, npCb, npCr, Y_shape, Chroma_shape

def frame_420_to_np_shaped(frame):
	frame_number, npY, npCb, npCr, Y_shape, Chroma_shape = frame_420_to_np(frame)
	npY = np.reshape(npY, Y_shape)
	npCb = np.reshape(npCb, Chroma_shape)
	npCr = np.reshape(npCr, Chroma_shape)

	return frame_number, npY, npCb, npCr


def show_component_frame(component_pels, shape, filename=None):
	component_pels = np.reshape(component_pels, shape)
	plt.figure()
	plt.imshow(component_pels, cmap="gray", aspect="auto")
	plt.show(block=False)
	if filename != None: plt.savefig(frames_pathstr + filename + '.pdf')

def show_YCbCr_frame(Y, Cb, Cr, Y_shape, Chroma_shape=None, filename=None):
	if Chroma_shape == None: Chroma_shape = Y_shape
	if filename == None:
		show_component_frame(Y, Y_shape)
		show_component_frame(Cb, Chroma_shape)
		show_component_frame(Cr, Chroma_shape)
	else:
		show_component_frame(Y, Y_shape, filename + "_Y")
		show_component_frame(Cb, Chroma_shape, filename + "_Cb")
		show_component_frame(Cr, Chroma_shape, filename + "_Cr")


def saveFig_component_frame(component_pels, shape, filename=""):
	component_pels = np.reshape(component_pels, shape)
	plt.figure()
	plt.imshow(component_pels, cmap="gray", aspect="auto")
	plt.savefig(frames_pathstr + filename + '.pdf')
	plt.close()

def saveFig_component_frame_shaped(component_pels, filename=""):
	plt.figure()
	plt.imshow(component_pels, cmap="gray", aspect="auto")
	plt.savefig(frames_pathstr + filename + '.pdf')
	plt.close()


def saveFig_YCbCr_frame(Y, Cb, Cr, Y_shape, Chroma_shape=None, filename=""):
	if Chroma_shape == None: Chroma_shape = Y_shape
	saveFig_component_frame(Y, Y_shape, filename + "_Y")
	saveFig_component_frame(Cb, Chroma_shape, filename + "_Cb")
	saveFig_component_frame(Cr, Chroma_shape, filename + "_Cr")


def save_frame(frame, video_name = ""):
	frame_number, Y, Cb, Cr, Y_shape, Chroma_shape = frame_420_to_np(frame)
	saveFig_YCbCr_frame(Y, Cb, Cr, Y_shape, Chroma_shape, video_name + "_" + str(frame_number))

def save_frameY(frame, video_name = ""):
	frame_number, Y, Cb, Cr, Y_shape, Chroma_shape = frame_420_to_np(frame)
	saveFig_component_frame(Y, Y_shape, video_name + "_" + str(frame_number))

def get_frameY(frame):
	frame_number, Y, Cb, Cr, Y_shape, Chroma_shape = frame_420_to_np(frame)
	return Y, Y_shape

def get_frameY_shaped(frame):
	frame_number, Y, Cb, Cr = frame_420_to_np_shaped(frame)
	return Y



def get_video_frames(video_name):
	frames_list = []
	# print(video_name)
	# print(len(frames_list))

	def get_frame(frame):
		frames_list.append(frame)

	# parser = y4m.Reader(frame_to_list, verbose=False)
	parser = y4m.Reader(get_frame, verbose=False)
	
	video_file = io.open(VIDEO_DIRECTORY + video_name + ".y4m", "rb")
	# print(video_file)
	with video_file as f:
		while True:
			# print("reading data")
			data = f.read(1024)
			if not data:
				# print("end of data!")
				break
			parser.decode(data)
	# print(len(frames_list))
	return frames_list


def get_interpol_sets(video_list):
	# input_list1 = []
	# input_list2 = []
	input_list = []
	output_list = []

	# for video in video_list:
	for i, video in enumerate(video_list):
		print("\t" + video)
		print("\t" + video, file=logfile)

		frame_list = get_video_frames(video)
		frame_list_size = len(frame_list)

		print("\t\tGot " + str(frame_list_size) + " frames")
		print("\t\tGot " + str(frame_list_size) + " frames", file=logfile)


		init_f_idx = random.randint(0,frame_list_size - 4) # have to have 2 frames after init_f_idx
		sub_frames_list = frame_list[init_f_idx : init_f_idx+3]


		input_data1 = get_frameY_shaped(sub_frames_list[0])
		input_data2 = get_frameY_shaped(sub_frames_list[2])
		input_data = np.stack((input_data1, input_data2), axis=-1)
		output_data = get_frameY_shaped(sub_frames_list[1]) # middle frame for interpolation


		# saveFig_component_frame_shaped(input_data[:, :, 0], video + "_prev")
		# saveFig_component_frame_shaped(input_data[:, :, 1], video + "_post")
		# saveFig_component_frame_shaped(output_data, video + "_inter")


		# input_list1.append(input_data1)
		# input_list2.append(input_data2)
		input_list.append(input_data)
		output_list.append(output_data)

		print("")
		print("", file=logfile)

	return np.array(input_list), np.array(output_list)
	# return np.array(input_list1), np.array(input_list2), np.array(output_list)

def get_interpol_sets_scaled(video_list):
	input_list = []
	output_list = []

	for video in video_list:
		print("\t" + video)
		print("\t" + video, file=logfile)

		frame_list = get_video_frames(video)
		frame_list_size = len(frame_list)

		print("\t\tGot " + str(frame_list_size) + " frames")
		print("\t\tGot " + str(frame_list_size) + " frames", file=logfile)


		# init_f_idx = random.randint(0,frame_list_size - 4) # have to have 2 frames after init_f_idx
		# sub_frames_list = frame_list[init_f_idx : init_f_idx+3]
		init_f_idx = random.randrange(0,frame_list_size - 4) # have to have 3 frames after init_f_idx
		sub_frames_list = frame_list[init_f_idx : init_f_idx+4]


		input_data1 = get_frameY_shaped(sub_frames_list[0])/255
		# input_data2 = get_frameY_shaped(sub_frames_list[2])/255
		input_data2 = get_frameY_shaped(sub_frames_list[1])/255
		input_data3 = get_frameY_shaped(sub_frames_list[3])/255
		# input_data = np.stack((input_data1, input_data2), axis=-1)
		input_data = np.stack((input_data1, input_data2, input_data3), axis=-1)

		# output_data = get_frameY_shaped(sub_frames_list[1])/255 # middle frame for interpolation
		output_data = get_frameY_shaped(sub_frames_list[2])/255 # middle frame for interpolation


		# saveFig_component_frame_shaped(input_data[:, :, 0]*255, video + "_prev")
		# saveFig_component_frame_shaped(input_data[:, :, 1]*255, video + "_post")
		# saveFig_component_frame_shaped(output_data*255, video + "_inter")

		input_list.append(input_data)
		output_list.append(output_data)

		print("")
		print("", file=logfile)

	return np.array(input_list), np.array(output_list)




def get_data_sets_scaled(video_list):
	data_list = []

	for video in video_list:
		print("\t" + video)
		print("\t" + video, file=logfile)

		frame_list = get_video_frames(video)
		frame_list_size = len(frame_list)

		print("\t\tGot " + str(frame_list_size) + " frames")
		print("\t\tGot " + str(frame_list_size) + " frames", file=logfile)


		frame_idx = random.randrange(0, frame_list_size)
		sub_frames_list = [frame_list[frame_idx]]

		for frame in sub_frames_list:
			tmp_data = get_frameY_shaped(frame)/255
			# data_list.append(get_frameY_shaped(frame)/255)
			# data_list.append([get_frameY_shaped(frame)/255])
			# data_list.append([tmp_data, tmp_data])
			data_list.append(np.stack((tmp_data, tmp_data), axis=-1))
			# saveFig_component_frame_shaped(data_list[-1]*255, video + "_" + str(frame_idx))

		print("")
		print("", file=logfile)

	return np.array(data_list)










def get_interpol_neural_model():
	model = Sequential(
		# [
		# 	keras.Input(shape=INPUT_SHAPE),
		# 	layers.Flatten(),
		# 	# layers.Dense(256, activation=activ_funct),
		# 	layers.Dense(1024, activation=activ_funct),
		# 	# layers.Dense(2048, activation=activ_funct),
		# 	layers.Dropout(droput_rate),
		# 	# layers.Dense(256, activation=activ_funct),
		# 	layers.Dense(1024, activation=activ_funct),
		# 	# layers.Dense(2048, activation=activ_funct),
		# 	layers.Dropout(droput_rate),
		# 	layers.Dense(FRAME_SIZE, activation=activ_last_funct),
		# 	layers.Reshape(OUTPUT_SHAPE)
		# ]
		# [
		# 	keras.Input(shape=INPUT_SHAPE),
		# 	layers.Conv2D(32, kernel_size=(3, 3), activation=activ_funct),
		# 	layers.MaxPooling2D(pool_size=(2, 2)),
		# 	layers.Conv2D(64, kernel_size=(3, 3), activation=activ_funct),
		# 	layers.MaxPooling2D(pool_size=(2, 2)),
		# 	layers.Conv2D(128, kernel_size=(3, 3), activation=activ_funct),
		# 	layers.MaxPooling2D(pool_size=(2, 2)),
		# 	layers.Conv2D(64, kernel_size=(3, 3), activation=activ_funct),
		# 	layers.MaxPooling2D(pool_size=(2, 2)),
		# 	layers.Flatten(),
		# 	layers.Dropout(droput_rate),
		# 	# layers.Dense(512, activation=activ_funct),
		# 	layers.Dense(1024, activation=activ_funct),
		# 	layers.Dropout(droput_rate),
		# 	layers.Dense(1024, activation=activ_funct),
		# 	layers.Dropout(droput_rate),
		# 	layers.Dense(FRAME_SIZE, activation=activ_last_funct),
		# 	layers.Reshape(OUTPUT_SHAPE)
		# ]
		# [
		# 	keras.Input(shape=INPUT_SHAPE),
		# 	layers.Conv2D(64, kernel_size=(3, 3), activation=activ_funct),
		# 	layers.MaxPooling2D(pool_size=(2, 2)),
		# 	layers.Conv2D(128, kernel_size=(3, 3), activation=activ_funct),
		# 	layers.MaxPooling2D(pool_size=(2, 2)),
		# 	layers.Conv2D(256, kernel_size=(3, 3), activation=activ_funct),
		# 	layers.MaxPooling2D(pool_size=(2, 2)),
		# 	layers.Conv2D(128, kernel_size=(3, 3), activation=activ_funct),
		# 	layers.MaxPooling2D(pool_size=(2, 2)),
		# 	layers.Conv2D(64, kernel_size=(3, 3), activation=activ_funct),
		# 	layers.MaxPooling2D(pool_size=(2, 2)),
		# 	layers.Conv2D(32, kernel_size=(3, 3), activation=activ_funct),
		# 	layers.MaxPooling2D(pool_size=(2, 2)),
		# 	layers.Flatten(),
		# 	layers.Dense(FRAME_SIZE, activation=activ_last_funct),
		# 	layers.Reshape(OUTPUT_SHAPE)
		# ]
		# [
		# 	keras.Input(shape=INPUT_SHAPE),
		# 	layers.Conv2D(128, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Conv2D(64, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	# layers.Conv2D(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	# layers.Conv2DTranspose(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Conv2DTranspose(64, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Conv2DTranspose(128, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	# layers.Conv2D(3, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Conv2D(1, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# ]
		# [
		# 	keras.Input(shape=INPUT_SHAPE),
		# 	layers.Conv2D(64, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Conv2D(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Conv2DTranspose(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Conv2DTranspose(64, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	# layers.Conv2D(3, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Conv2D(1, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# ]
		# [
		# 	keras.Input(shape=INPUT_SHAPE),
		# 	# layers.Conv2D(64, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	# layers.Conv2D(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	# layers.Conv2DTranspose(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	# layers.Conv2DTranspose(64, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	# layers.Conv2D(3, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Conv2D(1, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# ]
		[
			keras.Input(shape=INPUT_SHAPE),
			layers.Conv2D(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
			# layers.Dropout(droput_rate),
			layers.Conv2DTranspose(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
			# layers.Dropout(droput_rate),
			layers.SeparableConv2D(1, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		]
		# [
		# 	keras.Input(shape=INPUT_SHAPE),
		# 	layers.Conv2D(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Dropout(droput_rate),
		# 	layers.MaxPooling2D(pool_size=(2, 2)),
		# 	# layers.AveragePooling2D(pool_size=(2, 2)),
		# 	layers.Conv2D(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Dropout(droput_rate),
		# 	layers.Conv2DTranspose(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Dropout(droput_rate),
		# 	layers.UpSampling2D((2, 2)),
		# 	layers.Conv2DTranspose(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# 	layers.Dropout(droput_rate),
		# 	layers.SeparableConv2D(1, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		# ]
	)

	model.compile(
		optimizer=Adam(learning_rate=lr),
		loss=loss_funct,
		metrics=['accuracy', "mean_squared_error"],
	)
	return model


def get_frame_neural_model():
	model = Sequential(
		# [
		# 	keras.Input(shape=INPUT_SHAPE),
		# 	layers.Conv1D(INPUT_SHAPE[1], kernel_size=3, activation=activ_funct, padding="same"),
		# 	layers.Conv1D(128, kernel_size=3, activation=activ_funct, padding="same"),
		# 	layers.Conv1D(64, kernel_size=3, activation=activ_funct, padding="same"),
		# 	# layers.Conv1D(32, kernel_size=3, activation=activ_funct, padding="same"),
		# 	# layers.Conv1DTranspose(32, kernel_size=3, activation=activ_funct, padding="same"),
		# 	layers.Conv1DTranspose(64, kernel_size=3, activation=activ_funct, padding="same"),
		# 	layers.Conv1DTranspose(128, kernel_size=3, activation=activ_funct, padding="same"),
		# 	layers.Conv1DTranspose(INPUT_SHAPE[1], kernel_size=3, activation=activ_funct, padding="same"),
		# ]
		[
			keras.Input(shape=INPUT_SHAPE),
			layers.Conv2D(64, kernel_size=(3, 3), activation=activ_funct, padding="same"),
			layers.Conv2D(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
			layers.Conv2DTranspose(32, kernel_size=(3, 3), activation=activ_funct, padding="same"),
			layers.Conv2DTranspose(64, kernel_size=(3, 3), activation=activ_funct, padding="same"),
			# layers.Conv2D(3, kernel_size=(3, 3), activation=activ_funct, padding="same"),
			layers.Conv2D(1, kernel_size=(3, 3), activation=activ_funct, padding="same"),
		]
	)

	model.compile(
		optimizer=Adam(learning_rate=lr),
		loss=loss_funct,
		metrics=['accuracy', "mean_squared_error"],
	)
	return model




def write_history(history):
	acc = history.history['accuracy']
	val_acc = history.history['val_accuracy']
	loss = history.history['loss']
	val_loss = history.history['val_loss']
	mean_squared_error = history.history['mean_squared_error']
	val_mean_squared_error = history.history['val_mean_squared_error']

	last_epoch = len(loss)
	epochs = range(1, last_epoch + 1)

	print("\nNumber of Epochs = " + str(last_epoch), file = logfile)
	print("Last Epoch Stats:", file = logfile)
	print("\tacc = " + str(acc[last_epoch - 1]), file = logfile)
	print("\tval_acc = " + str(val_acc[last_epoch - 1]), file = logfile)
	print("\tloss = " + str(loss[last_epoch - 1]), file = logfile)
	print("\tval_loss = " + str(val_loss[last_epoch - 1]), file = logfile)
	print("\tmean_squared_error = " + str(mean_squared_error[last_epoch - 1]), file = logfile)
	print("\tval_mean_squared_error = " + str(val_mean_squared_error[last_epoch - 1]), file = logfile)

	print("\n", file = logfile)

	plt.figure()
	plt.xlabel('Epoch')
	plt.ylabel('Loss')
	plt.plot(history.history['loss'], label='train')
	plt.plot(history.history['val_loss'], label='test')
	plt.legend()
	# plt.show(block=False)
	plt.savefig(pathstr + 'loss_history' + '.pdf')

	plt.figure()
	plt.xlabel('Epoch')
	plt.ylabel('Accuracy')
	plt.plot(history.history['accuracy'], label='train')
	plt.plot(history.history['val_accuracy'], label='test')
	plt.legend()
	# plt.show(block=False)
	plt.savefig(pathstr + 'acc_history' + '.pdf')

	plt.figure()
	plt.xlabel('Epoch')
	plt.ylabel('Mean Squared Error')
	plt.plot(history.history['mean_squared_error'], label='train')
	plt.plot(history.history['val_mean_squared_error'], label='test')
	plt.legend()
	# plt.show(block=False)
	plt.savefig(pathstr + 'mse_history' + '.pdf')


if __name__ == '__main__':
	# print(len(VIDEO_LIST))
	tmp_video_list, testing_video_list = train_test_split(VIDEO_LIST, random_state=1, test_size=TESTING_SIZE)
	training_video_list, validating_video_list = train_test_split(tmp_video_list, random_state=2, test_size=VALIDATING_SIZE)

	# print(len(training_video_list))
	# print(len(validating_video_list))
	# print(len(testing_video_list))
	# print("\n\n")

	print(training_video_list)
	print(training_video_list, file=logfile)

	print(validating_video_list)
	print(validating_video_list, file=logfile)

	print(testing_video_list)
	print(testing_video_list, file=logfile)


	assert(len(training_video_list) == TRAINING_SIZE)
	assert(len(validating_video_list) == VALIDATING_SIZE)
	assert(len(testing_video_list) == TESTING_SIZE)

	print("")
	print("", file=logfile)

	if INTERPOL_MODE == None:
		np_train_in, np_train_out = get_interpol_sets(training_video_list)
		print("Got training_video_list\n")
		print("Got training_video_list\n", file=logfile)

		np_val_in, np_val_out = get_interpol_sets(validating_video_list)
		print("Got validating_video_list\n")
		print("Got validating_video_list\n", file=logfile)


		np_test_in, np_test_out = get_interpol_sets(testing_video_list)
		print("Got testing_video_list\n")
		print("Got testing_video_list\n", file=logfile)

		for i, frame in enumerate(np_train_out): saveFig_component_frame_shaped(frame, training_video_list[i] + "_train_out")
		for i, frame in enumerate(np_val_out): saveFig_component_frame_shaped(frame, validating_video_list[i] + "_val_out")
		for i, frame in enumerate(np_test_out): saveFig_component_frame_shaped(frame, testing_video_list[i] + "_test_out")

	elif INTERPOL_MODE:
		if SCALING: np_train_in, np_train_out = get_interpol_sets_scaled(training_video_list)
		else: np_train_in, np_train_out = get_interpol_sets(training_video_list)
		# np_train_in1, np_train_in2, np_train_out = get_interpol_sets(training_video_list)
		print("Got training_video_list\n")
		print("Got training_video_list\n", file=logfile)

		if SCALING: np_val_in, np_val_out = get_interpol_sets_scaled(validating_video_list)
		else: np_val_in, np_val_out = get_interpol_sets(validating_video_list)
		# np_val_in1, np_val_in2, np_val_out = get_interpol_sets(validating_video_list)
		print("Got validating_video_list\n")
		print("Got validating_video_list\n", file=logfile)


		model = get_interpol_neural_model()
		model.summary()
		with redirect_stdout(logfile):
			print('\n\nSummary:\n')
			model.summary()
			print('\n\n')


		print("\nParameters:", file = logfile)
		print("\tlr = " + str(lr), file = logfile)
		print("\tloss_funct = " + str(loss_funct), file = logfile)
		print("\tdroput_rate = " + str(droput_rate), file = logfile)
		print("\tactiv_funct = " + str(activ_funct), file = logfile)
		print("\tactiv_last_funct = " + str(activ_last_funct), file = logfile)
		print("\tepochs_val = " + str(epochs_val), file = logfile)
		print("\tbatch_size_val = " + str(batch_size_val), file = logfile)


		print("\n", file = logfile)
		print("Using callbacks:", file = logfile)
		print("\tmonitor_str = " + str(monitor_str), file = logfile)
		print("\tpatience_val = " + str(patience_val), file = logfile)
		print("\n", file = logfile)




		callbacks = keras.callbacks.EarlyStopping(monitor=monitor_str, patience=patience_val)

		t0 = time.clock()
		history = model.fit(np_train_in, np_train_out, validation_data = (np_val_in, np_val_out), epochs=epochs_val, batch_size=batch_size_val, callbacks=callbacks)
		t1 = time.clock()
		time_fit = t1-t0
		print("\ntime_fit = " + str(time_fit))
		print("\ntime_fit = " + str(time_fit), file = logfile)


		write_history(history)



		if SCALING: np_test_in, np_test_out = get_interpol_sets_scaled(testing_video_list)
		else: np_test_in, np_test_out = get_interpol_sets(testing_video_list)
		# np_test_in1, np_test_in2, np_test_out = get_interpol_sets(testing_video_list)
		print("Got testing_video_list\n")
		print("Got testing_video_list\n", file=logfile)

		TestPredictions = model.predict(np_test_in)

		# for pred_frame in TestPredictions:
		for i, pred_frame in enumerate(TestPredictions):
			if SCALING:
				saveFig_component_frame_shaped(pred_frame*255, "test_" + str(i) + "_predicted")
				saveFig_component_frame_shaped(np_test_out[i, :, :]*255, "test_" + str(i) + "_actual")
			else:
				saveFig_component_frame_shaped(pred_frame, "test_" + str(i) + "_predicted")
				saveFig_component_frame_shaped(np_test_out[i, :, :], "test_" + str(i) + "_actual")

	else:
		np_train = get_data_sets_scaled(training_video_list)
		print("Got training_video_list\n")
		print("Got training_video_list\n", file=logfile)

		np_val = get_data_sets_scaled(validating_video_list)
		print("Got validating_video_list\n")
		print("Got validating_video_list\n", file=logfile)

		print(np_val.shape)
		print(np_val.shape, file=logfile)
		print(np_val[0].shape)
		print(np_val[0].shape, file=logfile)
		print("\n")
		print("\n", file=logfile)
		# assert(False)


		model = get_frame_neural_model()
		model.summary()
		with redirect_stdout(logfile):
			print('\n\nSummary:\n')
			model.summary()
			print('\n\n')


		print("\nParameters:", file = logfile)
		print("\tlr = " + str(lr), file = logfile)
		print("\tloss_funct = " + str(loss_funct), file = logfile)
		print("\tdroput_rate = " + str(droput_rate), file = logfile)
		print("\tactiv_funct = " + str(activ_funct), file = logfile)
		print("\tactiv_last_funct = " + str(activ_last_funct), file = logfile)
		print("\tepochs_val = " + str(epochs_val), file = logfile)
		print("\tbatch_size_val = " + str(batch_size_val), file = logfile)


		print("\n", file = logfile)
		print("Using callbacks:", file = logfile)
		print("\tmonitor_str = " + str(monitor_str), file = logfile)
		print("\tpatience_val = " + str(patience_val), file = logfile)
		print("\n", file = logfile)


		callbacks = keras.callbacks.EarlyStopping(monitor=monitor_str, patience=patience_val)

		t0 = time.clock()
		history = model.fit(np_train, np_train[:, :, :, 0], validation_data = (np_val, np_val[:, :, :, 0]), epochs=epochs_val, batch_size=batch_size_val, callbacks=callbacks)
		t1 = time.clock()
		time_fit = t1-t0
		print("\ntime_fit = " + str(time_fit))
		print("\ntime_fit = " + str(time_fit), file = logfile)


		write_history(history)



		np_test = get_data_sets_scaled(testing_video_list)
		print("Got testing_video_list\n")
		print("Got testing_video_list\n", file=logfile)

		TestPredictions = model.predict(np_test)

		# for pred_frame in TestPredictions:
		for i, pred_frame in enumerate(TestPredictions):
			# saveFig_component_frame_shaped(pred_frame[:,:,0]*255, "test_" + str(i) + "_predicted")
			saveFig_component_frame_shaped(pred_frame[:,:]*255, "test_" + str(i) + "_predicted")
			# saveFig_component_frame_shaped(np_test[i, :, :]*255, "test_" + str(i) + "_actual")
			saveFig_component_frame_shaped(np_test[i, :, :, 0]*255, "test_" + str(i) + "_actual")



	print("\nsuccess!")
	# print(input())

	logfile.close()
	plt.show(block=WAIT_PLOTS)